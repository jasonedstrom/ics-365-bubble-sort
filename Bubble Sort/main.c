//
//  main.c
//  Bubble Sort
//
//  Created by Jason Edstrom on 3/4/14.
//  Copyright (c) 2014 Jason Edstrom. All rights reserved.
//

#include <stdio.h>

/*
 *  reads a list of integers from the keyboard and enters them into array
 *  parameter: list - the array
 */
int read_integers(int list[]);


/*
 *  starts the sorting based on sort type and iterates through the arrays
 *  parameter: list - the array
 *  parameter: size - the length of the array
 *  parameter: sort - whether the order is ascending or descending
 */

void bubblesort(int list[], int size, char *sort);

/*
 *  starts the sorting based on sort type and iterates through the arrays
 *  parameter: list - the array
 *  parameter: index - current placement in the array.
 *  parameter: sort - whether the order is ascending or descending
 *  parameter: item1 - first value for the placement swap in the array.
 *  parameter: item2 - second value for the placement swap in the array.
*/
void swap(int item1, int item2, int list[], char *sort, int index);

const char up[5] = "up";
const char down[5] = "down";

int main(int argc, const char * argv[])
{
    int list[100];
    int size = 0;
    //int results;
    
    char sort[5];
    printf("Enter the sort order: ");
    scanf("%s", sort);
    
    
    printf("Enter your sequence of number (separated by spaces): ");
    
    size = read_integers(list);
    
    
    bubblesort(list, size, sort);
    
    if (strcmp(sort, up) == 0){
        printf("Bubblesort in ascending order: ");
        for( int j = 0; j < size; j++){
            printf("%d ", list[j]);
        }
    }  else if (strcmp(sort, down) == 0){
        printf("Bubblesort in descending order: ");
        for( int j = 0; j < size; j++){
            printf("%d ", list[j]);
    }
    }
    return 0;
}

int read_integers(int list[])
    {
    int i = 0;
    int num;
    
    //Reads and stores until 0 is found
    while (scanf("%i",&num) > 0){
        
        if (num > 0){
            list[i] = num;
            i++;
        } else if (num == 0){
            break;
        }
    }
    
    return i;
    
}




void bubblesort(int list[], int size, char *sort){
    
   
    for (int j = 0; j < size - 1; j++){
    for (int index = 0; index < size - 1; index++){
        
        if (strcmp(sort, up) == 0){
        if (list[index] > list[index + 1]){
            swap(list[index], list[index+1], list, sort, index);
        }
       }  else if (strcmp(sort, down) == 0){
           if (list[index] < list[index + 1]){
                swap(list[index], list[index+1], list, sort, index);
            }
        }
    }
}
    
}


void swap(int item1, int item2, int list[], char *sort, int index){
        if (strcmp(sort, up) == 0){
        
        list[index] = item2;
        list[index +1] = item1;
    }  else if (strcmp(sort, down) == 0){
        
        list[index+1] = item1;
        list[index] = item2;
    }
    
}